using System.Data; 
namespace eshop_express_backend.Helper;

public interface ISqlConnectionProvider
{
    Task<IDbConnection> GetConnection(string? connectionName = null);
}