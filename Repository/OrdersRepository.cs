using AutoMapper;
using AutoMapper.Internal;
using Dapper;
using eshop_express_backend.Helper;
using eshop_express_backend.Model;
using eshop_express_backend.Repository.Interface;

namespace eshop_express_backend.Repository;

public class OrdersRepository : IOrdersRepository
{
    private readonly ISqlConnectionProvider _sqlConnectionProvider;

    public OrdersRepository(ISqlConnectionProvider sqlConnectionProvider, IMapper mapper)
    {
        _sqlConnectionProvider = sqlConnectionProvider;
    }

    public async Task<OrderItem> CreateOrder(OrderItem orderItem)
    { 
        const string createOrderQuery = @"INSERT INTO ""order""(address,
                                                                payment,
                                                                user_id)
                                        VALUES(:Address,
                                               :Payment,
                                               :UserId)
                                                RETURNING id";

        const string createOrderArticlesQuery = @"INSERT INTO ordered_articles (article_id,
                                                                                order_id)
                                                    VALUES(:ArticleId,
                                                           :OrderId)";

        const string getOrderQuery =
            @"SELECT p.firstname,
            p.lastname,
            o.id,
            o.address,
            o.payment,
            o.creation_time,
            o.delivery_time,
            o.user_id,
            a.name,
            a.image,
            a.description,
            a.price FROM ""order"" o
            INNER JOIN ordered_articles oa
            ON o.id = oa.order_id
            INNER JOIN articles a
            ON oa.article_id = a.id
            JOIN persons p ON o.user_id = p.user_id
            WHERE o.id = :OrderId";

        using var connection = await _sqlConnectionProvider.GetConnection();

        var orderId = await connection.QueryFirstOrDefaultAsync<int>(createOrderQuery,
            new { orderItem.Address, orderItem.Payment, orderItem.UserId });

        foreach (var article in orderItem.OrderedArticles)
        {
            await connection.ExecuteAsync(createOrderArticlesQuery, new { ArticleId = article.Id, OrderId = orderId });
        }

        var orders = new Dictionary<int, OrderItem>();

        await connection.QueryAsync<OrderItem, ArticleItem, OrderItem>(getOrderQuery, (orderItem, articleItem) =>
        {
            if (!orders.TryGetValue(orderItem.Id, out var orderEntry))
            {
                orderEntry = orderItem;
                orders.Add(orderEntry.Id, orderEntry);
            }

            orderEntry.OrderedArticles.TryAdd(articleItem);

            return null;
        }, splitOn: "name", param: new { OrderId = orderId });
        return orders.Values.First();
    }

    public async Task<OrderItem> GetOrderById(int orderId)
    {
        const string getOrderQuery =
            @"SELECT p.firstname,
            p.lastname, 
            o.id, 
            o.address,
            o.payment,
            o.creation_time,
            o.delivery_time,
            o.user_id,
            a.name,
            a.image,
            a.description,
            oa.id,
            a.price FROM ""order"" o
            INNER JOIN ordered_articles oa
            ON o.id = oa.order_id
            INNER JOIN articles a
            ON oa.article_id = a.id
            JOIN persons p 
            ON o.user_id = p.user_id
            WHERE o.id = :OrderId";

        using var connection = await _sqlConnectionProvider.GetConnection();

        var orders = new Dictionary<int, OrderItem>();

        await connection.QueryAsync<OrderItem, ArticleItem, OrderItem>(getOrderQuery,
            (orderItem, articleItem) =>
            {
                if (!orders.TryGetValue(orderItem.Id, out var orderEntry))
                {
                    orderEntry = orderItem;
                    orders.Add(orderEntry.Id, orderEntry);
                }

                orderEntry.OrderedArticles.TryAdd(articleItem);
                return null;
            }, splitOn: "name", param: new { OrderId = orderId });

        return orders.Values.First();
    }

    public async Task<List<OrderItem>> GetAllOrders()
    {
        const string getOrdersQuery =
            @"SELECT p.firstname,
            p.lastname,
            o.id,
            o.address,
            o.payment,
            o.creation_time,
            o.delivery_time,
            o.user_id,
            o.ordered_articles_id,
            oa.order_id,
            oa.article_id,
            oa.id,
            a.name,
            a.image,
            a.description,
            a.price
            FROM ""order"" o 
            INNER JOIN  ordered_articles oa
            ON oa.order_id = o.id 
            INNER JOIN articles a 
            ON oa.article_id = a.id 
            INNER JOIN persons p 
            on p.user_id = o.user_id";

        using var connection = await _sqlConnectionProvider.GetConnection();

        var orders = new Dictionary<int, OrderItem>();

        await connection.QueryAsync<OrderItem, ArticleItem, List<OrderItem>>(getOrdersQuery,
            (orderItem, articleItem) =>
            {
                if (!orders.TryGetValue(orderItem.Id, out var orderEntry))
                {
                    orderEntry = orderItem;
                    orderEntry.OrderedArticles = new List<ArticleItem>();
                    orders.Add(orderEntry.Id, orderEntry);
                }

                orderEntry.OrderedArticles.TryAdd(articleItem);


                return null;
            }, splitOn: "article_id");

        return orders.Values.ToList();
    }


    public async Task<ArticleItem> AddArticleToOrder(int articleId, string userId)
    {
        const string addArticleToOrderQuery =
            @"INSERT INTO ordered_articles (article_id,
                                            order_id)
            VALUES (:ArticleId,
                    :OrderId)";

        const string getOrderId = @"SELECT o.id
                                    FROM ""order"" o
                                    WHERE o.user_id = :UserId";

        const string getArticleById = @"SELECT a.id,
                                               a.image,
                                               a.name,
                                               a.description,
                                               a.price,
                                               a.amount,
                                               a.categorie
                                        FROM articles a
                                        WHERE id = :ArticleId";

        using var connection = await _sqlConnectionProvider.GetConnection();

        var order = await connection.QueryFirstOrDefaultAsync<OrderItem>(getOrderId, new { UserId = userId });

        await connection.QueryAsync<OrderedArticleItem>(addArticleToOrderQuery,
            new { ArticleId = articleId, OrderId = order.Id });

        var article =
            await connection.QueryFirstOrDefaultAsync<ArticleItem>(getArticleById, new { ArticleId = articleId });
        
        return article;
    }
}