using AutoMapper;
using Dapper;
using eshop_express_backend.Helper;
using eshop_express_backend.Model;
using eshop_express_backend.Repository.Interface;

namespace eshop_express_backend.Repository;

public class ShopRepository : IShopRepository
{
    private readonly ISqlConnectionProvider _sqlConnectionProvider;
    private readonly IMapper _mapper;


    public ShopRepository(ISqlConnectionProvider sqlConnectionProvider, IMapper mapper)
    {
        _sqlConnectionProvider = sqlConnectionProvider;
        _mapper = mapper;
    }

    public async Task<List<ArticleItem>> GetAllArticles()
    {
        const string query = @"SELECT * FROM articles";

        using var connection = await _sqlConnectionProvider.GetConnection();

        var articles = await connection.QueryAsync<ArticleItem>(query);

        return _mapper.Map<List<ArticleItem>>(articles);
    }

    public async Task<ArticleItem> GetArticleById(int articleId)
    {
        const string query = @"SELECT * FROM articles WHERE article_id = @id";

        using var conenction = await _sqlConnectionProvider.GetConnection();

        var article = await conenction.QueryFirstOrDefaultAsync<ArticleItem>(query, new { id = articleId });

        return article; 
    }


    public async Task<List<ArticleItem>> GetArticlesByCategorie(string categorie)
    {
        const string query = @"SELECT * FROM articles WHERE categorie = @categorieContent";

        using var connection = await _sqlConnectionProvider.GetConnection();

        var articles = await connection.QueryAsync<ArticleItem>(query, new { categorieContent = categorie });

        return _mapper.Map<List<ArticleItem>>(articles);
    }
}