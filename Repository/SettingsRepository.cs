using AutoMapper;
using Dapper;
using eshop_express_backend.Helper;
using eshop_express_backend.Model;
using eshop_express_backend.Repository.Interface;
using Microsoft.AspNetCore.Mvc.Filters;

namespace eshop_express_backend.Repository;

public class SettingsRepository : ISettingsRepository
{
    private readonly ISqlConnectionProvider _sqlConnectionProvider;
    private readonly IMapper _mapper;

    public SettingsRepository(ISqlConnectionProvider sqlConnectionProvider, IMapper mapper)
    {
        _sqlConnectionProvider = sqlConnectionProvider;
        _mapper = mapper;
    }
    
    public async Task<List<SettingsItem>> GetAllArticleFilter()
    {
        const string query = @"SELECT DISTINCT categorie  FROM articles";

        using var connection = await _sqlConnectionProvider.GetConnection();

        var filter = await connection.QueryAsync<SettingsItem>(query);

        return _mapper.Map<List<SettingsItem>>(filter);
    }
}