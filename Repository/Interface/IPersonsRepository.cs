using eshop_express_backend.Model;

namespace eshop_express_backend.Repository.Interface;

public interface IPersonsRepository
{
    public Task<PersonsItem> CreateNewPerson(PersonsItem personsItem);

    public Task<List<PersonsItem>> Get(string email); 
}