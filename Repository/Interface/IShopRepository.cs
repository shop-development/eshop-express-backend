using eshop_express_backend.Model;

namespace eshop_express_backend.Repository.Interface;

public interface IShopRepository
{
    public Task<List<ArticleItem>> GetAllArticles();

    public Task<ArticleItem> GetArticleById(int articleId); 

    public Task<List<ArticleItem>> GetArticlesByCategorie(string categorie); 
    
    

}