using eshop_express_backend.Model;
using Microsoft.AspNetCore.Mvc.Filters;

namespace eshop_express_backend.Repository.Interface;

public interface ISettingsRepository
{
    public Task<List<SettingsItem>> GetAllArticleFilter(); 
}