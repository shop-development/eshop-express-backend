using eshop_express_backend.Repository.Interface;
using System.Security.Cryptography;
using Dapper;
using eshop_express_backend.Helper;
using eshop_express_backend.Model;
using Random = System.Random;

namespace eshop_express_backend.Repository;

public class PersonsRepository : IPersonsRepository
{
    private readonly ISqlConnectionProvider _sqlConnectionProvider;
    
    public PersonsRepository(ISqlConnectionProvider sqlConnectionProvider)
    {
        _sqlConnectionProvider = sqlConnectionProvider;
    }
    
    public async Task<PersonsItem> CreateNewPerson(PersonsItem personsItem)
    {
        const string createPersonQuery = @"INSERT INTO persons (id,
                                                                email,
                                                                firstname,
                                                                lastname,
                                                                password,
                                                                creation_time) 
                                            VALUES (:PersonId,:Email, :Firstname, :Lastname, :Passwort, :CreationTime )";

        const string getNewPerson = @"SELECT p.id,
                                             p.firstname,
                                             p.lastname,
                                             p.email,
                                             p.password, 
                                             p.creation_time 
                                    FROM persons p WHERE p.id = :PersonId";


        using var connection = await _sqlConnectionProvider.GetConnection();

        var PersonId = GenerateUserId();

        await connection.QueryAsync(createPersonQuery,
            new
            {
                PersonId = PersonId, 
                Email = personsItem.Email, 
                Firstname = personsItem.Firstname, 
                Lastname = personsItem.Lastname, 
                Passwort = personsItem.Password,
                CreationTime = DateTime.Now
            });

        var person = await connection.QueryFirstOrDefaultAsync<PersonsItem>(getNewPerson, new { PersonId = PersonId });

        return person;
    }

    public async Task<List<PersonsItem>> Get(string email)
    {
        const string getPersonString = @"SELECT p.id,
                                                p.firstname,
                                                p.lastname,
                                                p.email,
                                                p.creation_time
                                                FROM persons p 
                                                WHERE p.email = :Email";


        using var connection = await _sqlConnectionProvider.GetConnection();

        var persons = await connection.QueryAsync<PersonsItem>(getPersonString, new { Email = email });

        return persons.ToList(); 
        
    }


    private string GenerateUserId()
    {
        Random random = new Random();

        while (true)
        {
            var generateId = random.Next(1000, 9999).ToString();
            
            if (generateId == null)
            {
                return null;
            }

            return generateId;
        }
    }
}