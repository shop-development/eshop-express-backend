using eshop_express_backend.Handler.Interface;
using eshop_express_backend.Model;
using eshop_express_backend.Repository.Interface;

namespace eshop_express_backend.Handler;

public class PersonsHandler: IPersonsHandler
{
    private readonly IPersonsRepository _personsRepository;

    public PersonsHandler(IPersonsRepository personsRepository)
    {
        _personsRepository = personsRepository;
    }
    
    public async Task<PersonsItem> CreateNewPerson(PersonsItem personsItem)
    {
        return await _personsRepository.CreateNewPerson(personsItem); 
    }

    public async Task<List<PersonsItem>> Get(string email)
    {
        return await _personsRepository.Get(email); 
    }
}