using eshop_express_backend.Handler.Interface;
using eshop_express_backend.Model;
using eshop_express_backend.Repository.Interface;

namespace eshop_express_backend.Handler;

public class ShopHandler: IShopHandler
{

    private readonly IShopRepository _shopRepository;


    public ShopHandler(IShopRepository shopRepository)
    {
        _shopRepository = shopRepository; 
    }
    
    public async Task<List<ArticleItem>> GetAllArticles()
    {
        return await _shopRepository.GetAllArticles(); 
    }

    public async Task<ArticleItem> GetArticleById(int articleId)
    {
        return await _shopRepository.GetArticleById(articleId); 
    }
    

    public async Task<List<ArticleItem>> GetArticlesByCategorie(string categorie)
    {
        return await _shopRepository.GetArticlesByCategorie(categorie); 
    }
    
}