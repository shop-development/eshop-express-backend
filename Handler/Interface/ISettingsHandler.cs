using eshop_express_backend.Model;
using Microsoft.AspNetCore.Mvc.Filters;

namespace eshop_express_backend.Handler.Interface;

public interface ISettingsHandler
{
    public Task<List<SettingsItem>> GetAllArticleFilter(); 
}