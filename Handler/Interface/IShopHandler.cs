using System.Data;
using eshop_express_backend.Model;

namespace eshop_express_backend.Handler.Interface;



public interface IShopHandler
{
    public Task<List<ArticleItem>> GetAllArticles();

    public Task<List<ArticleItem>> GetArticlesByCategorie(string categorie);

    public Task<ArticleItem> GetArticleById(int articleId); 


}