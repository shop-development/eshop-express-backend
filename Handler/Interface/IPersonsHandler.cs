using eshop_express_backend.Model;

namespace eshop_express_backend.Handler.Interface;

public interface IPersonsHandler
{
    public Task<PersonsItem> CreateNewPerson(PersonsItem personsItem);
    public Task<List<PersonsItem>> Get(string email); 
}