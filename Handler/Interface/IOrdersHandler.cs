using eshop_express_backend.Model;

namespace eshop_express_backend.Handler.Interface;

public interface IOrdersHandler
{
    public Task<OrderItem> CreateOrder(OrderItem orderItem);
    public Task<OrderItem> GetOrderById(int orderId);
    public Task<List<OrderItem>> GetAllOrders();
    public Task<ArticleItem> AddArticleToOrder(int articleId, string userId); 
}