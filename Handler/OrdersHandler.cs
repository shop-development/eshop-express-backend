using eshop_express_backend.Handler.Interface;
using eshop_express_backend.Model;
using eshop_express_backend.Repository.Interface;


namespace eshop_express_backend.Handler;

public class OrdersHandler : IOrdersHandler
{
    private readonly IOrdersRepository _ordersRepository;

    public OrdersHandler(IOrdersRepository ordersRepository)
    {
        _ordersRepository = ordersRepository;
    }

    public async Task<OrderItem> CreateOrder(OrderItem orderItem)
    {
        return await _ordersRepository.CreateOrder(orderItem);
    }

    public async Task<OrderItem> GetOrderById(int orderId)
    {
        return await _ordersRepository.GetOrderById(orderId); 
    }

    public async Task<List<OrderItem>> GetAllOrders()
    {
        return await _ordersRepository.GetAllOrders(); 
    }

    public async Task<ArticleItem> AddArticleToOrder(int articleId, string userId)
    {
        return await _ordersRepository.AddArticleToOrder(articleId, userId); 
    }
    
}