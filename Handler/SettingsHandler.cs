using eshop_express_backend.Handler.Interface;
using eshop_express_backend.Model;
using eshop_express_backend.Repository.Interface;
using Microsoft.AspNetCore.Mvc.Filters;

namespace eshop_express_backend.Handler;

public class SettingsHandler: ISettingsHandler
{

    private readonly ISettingsRepository _settingsRepository;

    public SettingsHandler(ISettingsRepository settingsRepository)
    {
        _settingsRepository = settingsRepository; 
    }
    
    
    public async Task<List<SettingsItem>> GetAllArticleFilter()
    {
        return await _settingsRepository.GetAllArticleFilter(); 
    }
}