namespace eshop_express_backend.Model;

public class OrderedArticleItem
{
    public int Id { get; set; }
    public int ArticleId { get; set; }
    public int OrderId { get; set; }
}