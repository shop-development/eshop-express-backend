namespace eshop_express_backend.Model;

public class PersonsItem
{
    public string Id { get; set; }
    public string Email { get; set; }
    public string Password { get; set; }
    public string Firstname { get; set; }
    public string Lastname { get; set; }
}