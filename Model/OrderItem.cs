using System.ComponentModel.DataAnnotations;

namespace eshop_express_backend.Model;

public class OrderItem
{
    public int Id { get; set; }
    public string CustomerName { get; set; }
    public string Firstname { get; set; }
    public string Lastname { get; set; }

    [Required(ErrorMessage = "UserId is missing!")]
    public int UserId { get; set; }

    [Required(ErrorMessage = "OrderedArticles is missing!")]
    public List<ArticleItem> OrderedArticles { get; set; } =  new List<ArticleItem>();

    [Required(ErrorMessage = "Adress is missing!")]
    public string Address { get; set; }

    [Required(ErrorMessage = "Payment is missing!")]
    public string Payment { get; set; }
}