using Microsoft.EntityFrameworkCore;

namespace eshop_express_backend.Model;

public class DatabaseContext: DbContext
{
    public DatabaseContext(DbContextOptions<DatabaseContext> options)
        : base(options)
    {
    }

    public DbSet<ArticleItem> LoginItems { get; set; } = null!;
}