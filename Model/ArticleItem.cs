using System.ComponentModel.DataAnnotations.Schema;
using System.Linq; 

namespace eshop_express_backend.Model;

public class ArticleItem
{
    public int Id { get; set; }
    public string Image { get; set; }
    public string Name { get; set; }
    public string Description { get; set; }
    public int Amount { get; set; }
    public double Price { get; set; }
    public string Categories { get; set; }
}