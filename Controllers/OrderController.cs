using eshop_express_backend.Handler.Interface;
using eshop_express_backend.Model;
using Microsoft.AspNetCore.Mvc;

namespace eshop_express_backend.Controllers;

[ApiController]
public class OrderController : ControllerBase
{
    private readonly IOrdersHandler _ordersHandler;


    public OrderController(IOrdersHandler ordersHandler)
    {
        _ordersHandler = ordersHandler;
    }


    [Route("api/orders")]
    [HttpPost]
    public async Task<ActionResult<OrderedArticleItem>> CreateOrder(OrderItem orderItem)
    {
        // user have to be login, else he cant get his order 
        
        var order = await _ordersHandler.CreateOrder(orderItem);

        if (order == null)
        {
            return BadRequest("Es ist ein Fehler beim erstellen einer Bestellung aufgetreten. Bitte versuchen Sie es erneut!");
        }

        return Ok(order);
    }


    [Route("api/orders")]
    [HttpGet]
    public async Task<ActionResult<List<OrderItem>>> GetAllOrders()
    {
        var orders = await _ordersHandler.GetAllOrders();


        if (orders.Count == 0)
        {
            return Ok(new { message = "Es konnten keine Bestellungen gefunden werden!" });
        }

        return Ok(orders);
    }


    [Route("api/order/{orderId}")]
    [HttpGet]
    public async Task<ActionResult<OrderItem>> GetOrderById(int orderId)
    {
        var order = await _ordersHandler.GetOrderById(orderId);

        if (order.Id == null)
        {
            return Ok(new
                { message = $"Es konnte keine Bestellung mit der Bestellungsnummer: {orderId} gefunden werden!" });
        }

        return Ok(order);
    }

    [HttpPost]
    [Route("api/order/{articleId}/{userId}")]
    public async Task<ActionResult<ArticleItem>> AddArticleToOrder(int articleId, string userId)
    {
        var orderedArticle = await _ordersHandler.AddArticleToOrder(articleId, userId);

        return Ok(orderedArticle);
    }
}