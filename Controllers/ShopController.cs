using eshop_express_backend.Handler.Interface;
using eshop_express_backend.Model;
using Microsoft.AspNetCore.Mvc;

namespace eshop_express_backend.Controllers;

[ApiController]
public class ShopController : ControllerBase
{
    private readonly IShopHandler _shopHandler;

    public ShopController(IShopHandler shopHandler)
    {
        _shopHandler = shopHandler;
    }

    [Route("api/Shop/GetAllArticles")]
    [HttpGet]
    public async Task<ActionResult<List<ArticleItem>>> GetAllArticles()
    {
        var articles = await _shopHandler.GetAllArticles();


        return StatusCode(200, articles);
    }

    [Route("api/Shop/GetArticleById")]
    [HttpGet]
    public async Task<ActionResult<ArticleItem>> GetArticleById(int articleId)
    {
        var article = await _shopHandler.GetArticleById(articleId);
        
        return StatusCode(200, article); 

    }

    [Route("api/Shop/GetArticlesByCategorie")]
    [HttpGet]
    public async Task<ActionResult<List<ArticleItem>>> GetArticlesByCategorie(string categorie)
    {
        var articles = await _shopHandler.GetArticlesByCategorie(categorie);

        return StatusCode(200, articles);
    }
    
}