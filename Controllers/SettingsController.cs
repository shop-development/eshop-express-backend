using eshop_express_backend.Handler.Interface;
using eshop_express_backend.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace eshop_express_backend.Controllers;

[ApiController]
public class SettingsController : ControllerBase
{
    private readonly ISettingsHandler _settingsHandler;

    public SettingsController(ISettingsHandler settingsHandler)
    {
        _settingsHandler = settingsHandler;
    }

    [Route("api/settings/articleFilter")]
    [HttpGet]
    public async Task<ActionResult<SettingsItem>> GetAllArticleFilter()
    {
        var articleFilter = await _settingsHandler.GetAllArticleFilter();

        return StatusCode(200, new { categories = articleFilter });
    }
}