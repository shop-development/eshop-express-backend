using eshop_express_backend.Handler.Interface;
using eshop_express_backend.Model;
using Microsoft.AspNetCore.Mvc;

namespace eshop_express_backend.Controllers;

public class PersonsController : ControllerBase
{
    private readonly IPersonsHandler _personsHandler;

    public PersonsController(IPersonsHandler personsHandler)
    {
        _personsHandler = personsHandler;
    }

    [Route("api/persons")]
    [HttpPost]
    public async Task<ActionResult<PersonsItem>> CreateNewPerson([FromBody] PersonsItem personsItem)
    {
        var existPerson = await _personsHandler.Get(personsItem.Email);

        if (existPerson.Count > 0)
        {
            return BadRequest(
                "Ein Nutzer mit der angegebenen Email Adresse existiert bereits, bitte verwende eine andere E-Mail Adresse oder melden Sie sich an.");
        }

        if (personsItem.Email == "")
        {
            return BadRequest("Bitte gib eine gültige E-Mail Adresse ein!");
        }

        if (personsItem.Password == "")
        {
            return BadRequest("Bitte gib ein gültiges Passwort ein!");
        }

        var person = await _personsHandler.CreateNewPerson(personsItem);


        return Ok(new { message = "Anmeldung erfolgreich!", person });
    }
}